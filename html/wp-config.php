<?php
define('WP_AUTO_UPDATE_CORE', 'minor');// This setting is required to make sure that WordPress updates can be properly managed in WordPress Toolkit. Remove this line if this WordPress website is not managed by WordPress Toolkit anymore.
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'roadtrip' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'secret' );

/** MySQL hostname */
define( 'DB_HOST', 'mariadb' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ' .c*~$0$hFa_p+`M0`|@C8<cZ|}Q;aZ5}8G>)#agJYgo;spCEqdI k!;&^[u#WzW' );
define( 'SECURE_AUTH_KEY',  'i%Zl1>;rc,@]-ALOqv~&%lw:;@ndF(hqO)h69@maau&oM^+1SxVX@Qrc(0u:y]a|' );
define( 'LOGGED_IN_KEY',    'D3}I!3=J]Yucw/+D68r0@:Gd))#2pRg}.5WLI~F6;lt#vt1Meab9po^cZ_)Ypngk' );
define( 'NONCE_KEY',        'pl+S}0:]aL5}z%pJS|*D-;+OH2(q<}m4qO(&;Y#T!v-%CbV,1Er&H[JX{Lq#4UYZ' );
define( 'AUTH_SALT',        't>j=6bwfU&.mq?w>-~+hj)iA[|w|-lXO{{G`FH3LwqkiaADBnS+B+G$X?}P:s8Y^' );
define( 'SECURE_AUTH_SALT', '9_ 8#XK^*oWQJJ_MV&b(3d74giIhR:xF]l.Y0~6k `-N F)v,-f+:W3qGS[8CqIM' );
define( 'LOGGED_IN_SALT',   '2)QNf3-5rl<xT;inCTNW&HWK.5Q+^h-UHl[<w({7R}YzQg$WW)pVCTun(0(?p^*@' );
define( 'NONCE_SALT',       '|UyN[3z&~t;1Jm(+R:;Y;sj8%g!r<yQa.n{=cqj0`{<{K7;gMjDyAlNO%.76T}(r' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
// define( 'WP_DEBUG', false );
// Enable WP_DEBUG mode
define( 'WP_DEBUG', true );

// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', true );

// Disable display of errors and warnings
define( 'WP_DEBUG_DISPLAY', true );
// @ini_set( 'display_errors', 0 );

define( 'AUTOSAVE_INTERVAL', 300 );
define( 'WP_POST_REVISIONS', 5 );
define( 'EMPTY_TRASH_DAYS', 7 );
define( 'WP_CRON_LOCK_TIMEOUT', 120 );
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
