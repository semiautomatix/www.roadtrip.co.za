=== Simple Page Redirect ===
Contributors : imemine
Tags: redirect,url,301,woocommerce,ecommerce,seo
Requires at least: 3.0
Tested up to: 5.5
Requires PHP: 5.0
Stable tag: trunk

This plugin allows you to make simple redirects of single pages of any custom post type to any url.

== Description ==
Redirect any post/page/custom post type/portfolio, to any internal or external url. This plugin adds an text option to single pages of all default and custom post types.


== Installation ==


    Upload Simple Page Redirect to the /wp-content/plugins/ directory

    Activate the plugin through the ‘Plugins’ menu in WordPress

    Edit any page and add destination URL to the text option metabox on the top right.
	
	For removing a redirect, simply blank out the field.