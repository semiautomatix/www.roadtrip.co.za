<?php
/* Template Name: thanku-page */

get_header();
?>

<div class="site-content" id="content">
<div class="main_container">
<div id="book-background-img" data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_custom_1584440615382 vc_row-has-fill vc_row-no-padding vc_row-o-equal-height vc_row-flex" style="position: relative; left: 15px; box-sizing: border-box; width: 1349px;"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><h2 style="font-size: 18px;color: #002237;line-height: 50px;text-align: center;font-family:Abril Fatface;font-weight:400;font-style:normal" class="vc_custom_heading book-heading vc_custom_1583230470635">book now for point to point TRIPS</h2><div class="vc_row wpb_row vc_inner vc_row-fluid book-heading padding-bottom vc_custom_1600770483857 vc_row-has-fill vc_row-o-equal-height vc_row-o-content-middle vc_row-flex"><div class="luxury-col wpb_column vc_column_container vc_col-sm-12" id="for_re_padding"><div class="vc_column-inner vc_custom_1600766103401"><div class="wpb_wrapper"><h2 style="font-size: 19px;color: #002a40;line-height: 7px;text-align: center;font-family:Abril Fatface;font-weight:400;font-style:normal" class="vc_custom_heading fillin-heading">THANK YOU FOR YOUR BOOKING</h2><div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_60 vc_sep_border_width_3 vc_sep_pos_align_center vc_separator_no_text vc_custom_1600769895789  vc_custom_1600769895789 separator-line"><span class="vc_sep_holder vc_sep_holder_l"><span style="border-color:#133347;" class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span style="border-color:#133347;" class="vc_sep_line"></span></span>
</div>
	<div class="wpb_text_column wpb_content_element  vc_custom_1600764853774 label-sec span-sec l-sec" id="label_id">
		<div class="wpb_wrapper">
			<p><label>Name:</label><span class="bg"><?php echo @$_GET['first-name']; ?></span></p>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  vc_custom_1600764869484 label-sec span-sec r-sec" id="label_id">
		<div class="wpb_wrapper">
			<p><label>Surname:</label><span class="bg"><?php echo @$_GET['last-name']; ?></span></p>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  vc_custom_1600764888256 label-sec span-sec l-sec" id="label_id">
		<div class="wpb_wrapper">
			<p><label>Email:</label><span class="bg"><?php echo @$_GET['emailaddress']; ?></span></p>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  vc_custom_1600764903733 label-sec span-sec r-sec" id="label_id">
		<div class="wpb_wrapper">
			<p><label>Cellphone:</label><span class="bg"><?php echo @$_GET['phonenumber']; ?></span></p>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  vc_custom_1600764913333 label-sec span-sec" id="label_id">
		<div class="wpb_wrapper">
			<p><label>Collection Date:</label><span class="bg"><?php echo @date('d F',strtotime($_GET['collection-date'])); ?></span></p>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  vc_custom_1600764926820 label-sec span-sec" id="label_id">
		<div class="wpb_wrapper">
			<p><label>Collection Time:</label><span class="bg"><?php echo @date('h m A',strtotime($_GET['collection-time'])); ?></span></p>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  vc_custom_1600764938010 label-sec span-sec" id="label_id">
		<div class="wpb_wrapper">
			<p><label>Pick up Point:</label><span class="bg"><?php echo @$_GET['pickup-address']; ?></span></p>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  vc_custom_1600764952984 label-sec span-sec" id="label_id">
		<div class="wpb_wrapper">
			<p><label>Drop Off Point:</label><span class="bg"><?php echo @$_GET['dropoff-address']; ?></span></p>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  vc_custom_1600764964913 label-sec span-sec" id="label_id">
		<div class="wpb_wrapper">
			<p><label>Newspaper and Bottled water:</label><span class="bg"><?php echo @$_GET['water']; ?></span></p>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  vc_custom_1600764985082 label-sec span-sec" id="label_id">
		<div class="wpb_wrapper">
			<p><label>Additional notes:</label><span class="bg"><?php echo @$_GET['Additionalnotes']; ?></span></p>

		</div>
	</div>
</div></div></div></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div>

	</div>

<!-- call footer -->

<?php

get_footer();

 ?>
<!-- footer end -->
	</div>