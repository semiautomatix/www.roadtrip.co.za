<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />	
	<meta property="og:title" content="Roadtrip" />
	<meta property="og:description" content="Road Trip has a membership based service that drives clients home in their own vehicles. Sign up as a member today and start using us from this evening. This is a sober solution to ensure you reach your destination safely & responsibly." />
	<meta property="og:image" content="http://roadtrip.co.za/wp-content/uploads/2020/02/airport-scaled.jpg" />

	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&amp;subset=greek-ext&amp;v2" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link href="//db.onlinewebfonts.com/c/a66cb603087c3d635836b1e7829a912e?family=TrendSansW00-One" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700italic,700,800,800italic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/fonts/trend-font/stylesheet.css">
	<link rel="https://cdn.rawgit.com/mfd/f3d96ec7f0e8f034cc22ea73b3797b59/raw/856f1dbb8d807aabceb80b6d4f94b464df461b3e/gotham.css">
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/font/gotham-book/stylesheet.css">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <!--Address autocomplete-->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyBfSF3lOIGalU5nUW9_R9JhcwR0DT54F84"></script>



			<script>
				// This example displays an address form, using the autocomplete feature
				// of the Google Places API to help users fill in the information.
				function initialize() {
				  // Create the autocomplete object, restricting the search
				  // to geographical location types.
				  
				  autocomplete1 = new google.maps.places.Autocomplete(
					  /** @type {HTMLInputElement} */(document.getElementById('address')),
					  { types: ['geocode','establishment'],
                        componentRestrictions: {country: "za"}});
				  // When the user selects an address from the dropdown,
				  // populate the address fields in the form.
				  google.maps.event.addListener(autocomplete1, 'place_changed', function() {
					fillInAddress(fromComponent, autocomplete1);
				  });  
				  
				  autocomplete2 = new google.maps.places.Autocomplete(
					  /** @type {HTMLInputElement} */(document.getElementById('address1')),
					  { types: ['geocode','establishment'],
                        componentRestrictions: {country: "za"}});
				  // When the user selects an address from the dropdown,
				  // populate the address fields in the form.
				  google.maps.event.addListener(autocomplete2, 'place_changed', function() {
					fillInAddress(fromComponent, autocomplete2);
				  }); 
				  
				  autocomplete3 = new google.maps.places.Autocomplete(
					  /** @type {HTMLInputElement} */(document.getElementById('address2')),
					  { types: ['geocode','establishment'],
                        componentRestrictions: {country: "za"}});
				  // When the user selects an address from the dropdown,
				  // populate the address fields in the form.
				  google.maps.event.addListener(autocomplete3, 'place_changed', function() {
					fillInAddress(fromComponent, autocomplete3);
				  }); 
				  
				  autocomplete4 = new google.maps.places.Autocomplete(
					  /** @type {HTMLInputElement} */(document.getElementById('address3')),
					  { types: ['geocode','establishment'],
                        componentRestrictions: {country: "za"}});
				  // When the user selects an address from the dropdown,
				  // populate the address fields in the form.
				  google.maps.event.addListener(autocomplete4, 'place_changed', function() {
					fillInAddress(fromComponent, autocomplete4);
				  }); 
		  
				}
				

				$( document ).ready(function() {
					initialize();
				});
			</script>			
  

<!--Address autocomplete-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentynineteen' ); ?></a>

		<header id="masthead" class="<?php echo is_singular() && twentynineteen_can_show_post_thumbnail() ? 'site-header featured-image' : 'site-header'; ?>">

			<div class="site-branding-container">
				<?php get_template_part( 'template-parts/header/site', 'branding' ); ?>
			</div><!-- .site-branding-container -->

			<?php if ( is_singular() && twentynineteen_can_show_post_thumbnail() ) : ?>
				<div class="site-featured-image">
					<?php
						twentynineteen_post_thumbnail();
						the_post();
						$discussion = ! is_page() && twentynineteen_can_show_post_thumbnail() ? twentynineteen_get_discussion_data() : null;

						$classes = 'entry-header';
					if ( ! empty( $discussion ) && absint( $discussion->responses ) > 0 ) {
						$classes = 'entry-header has-discussion';
					}
					?>
					<div class="<?php echo $classes; ?>">
						<?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
					</div><!-- .entry-header -->
					<?php rewind_posts(); ?>
				</div>
			<?php endif; ?>
		</header><!-- #masthead -->

	<div id="content" class="site-content">
		<div class="main_container">
