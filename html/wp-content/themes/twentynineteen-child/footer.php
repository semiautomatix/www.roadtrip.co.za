<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
	<?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?> 

		<div class="site-info">
			<?php $blog_info = get_bloginfo( 'name' ); ?>
			<?php if ( ! empty( $blog_info ) ) : ?>
				<a class="site-name" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>,
			<?php endif; ?>
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentynineteen' ) ); ?>" class="imprint">
				<?php
				/* translators: %s: WordPress. */
				printf( __( 'Proudly powered by %s.', 'twentynineteen' ), 'WordPress' );
				?>
			</a>
			<?php
			if ( function_exists( 'the_privacy_policy_link' ) ) {
				the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
			}
			?>
			<?php if ( has_nav_menu( 'footer' ) ) : ?>
				<nav class="footer-navigation" aria-label="<?php esc_attr_e( 'Footer Menu', 'twentynineteen' ); ?>">
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'footer',
							'menu_class'     => 'footer-menu',
							'depth'          => 3,
						)
					);
					?>
				</nav><!-- .footer-navigation -->
			<?php endif; ?>
		</div><!-- .site-info -->
		
		
		<!-- Button trigger modal -->
<!--<button type="button" >
  Launch demo modal
</button> -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog change-look" role="document">
    <div class="modal-content">
      <div class="modal-body">
       PAYMENT GATEWAY
      </div>
    </div>
  </div>
</div>
		
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>


<!-- <script>
	$('#wpcf7-f191-p20-o1 form').submit(function(){
		
		var pass = $(this).find('input[name="password"]').val();
		var cpass = $(this).find('input[name="CONFIRMPASSWORD"]').val();
	
		if(pass.trim() != cpass.trim()){
			console.log($(this).find('input[name="CONFIRMPASSWORD"]').parent('span'));
			$('.CONFIRMPASSWORD').append('<span role="alert" class="invalid-password" style="">password not same.</span>');
		
			return false;
		}else{
			$('.invalid-password').remove();
		}
		
	});

</script> -->
<script type="text/javascript">
    $('label.containe').find('input[type="checkbox"]').on('change',function(){
        var el = $('span.tcsaccept').find('input[type="checkbox"]');
        if($(this).is(":checked") == true){
               el.attr('checked',true);
        }else{
            el.removeAttr('checked');
        }
                
    });
	
</script>
<script>
jQuery(document).ready(function($){
	
$('#file-upload').change(function() {
  var label = $(this).parents('label.file_label').find('span.file_upload');
  var file = $('#file-upload')[0].files[0].name;
  label.text(file);
});
});
</script>
</body>
</html>
